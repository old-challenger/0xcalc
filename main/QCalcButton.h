#ifndef QT_CALC_BUTTON_H_MRV
#define QT_CALC_BUTTON_H_MRV

#include "bigNumber.h"
#include <QPushButton>
#include <map>
#include <cassert>

class QCalcButton : public QPushButton {
public:
   enum class valuesTypes_enum : bigNumber::element_type {
      VALUE_PLUS        = 0xf1,
      VALUE_MINUS       = 0xf2,
      VALUE_MULTIPLY    = 0xf3,
      VALUE_DIVIDE      = 0xf4,
      VALUE_RESULT      = 0xf5,
      VALUE_DIGIT       = 0xf6,
      VALUE_INVALID     = bigNumber::invalid,
   };

   QCalcButton( char value_p, QWidget * parent_p ) : QPushButton( QChar( value_p ), parent_p ) {
      static const std::map< char, valuesTypes_enum > operations_scm = {
         { '+', valuesTypes_enum::VALUE_PLUS       },
         { '-', valuesTypes_enum::VALUE_MINUS      }, 
         { '*', valuesTypes_enum::VALUE_MULTIPLY   }, 
         { '/', valuesTypes_enum::VALUE_DIVIDE     },
         { '=', valuesTypes_enum::VALUE_RESULT     }
      };
      //
      auto found_l = operations_scm.find( value_p );
      if ( operations_scm.end() != found_l ) {
         type_m = found_l->second;
         value_m = bigNumber::invalid;
      } else {
         type_m = valuesTypes_enum::VALUE_DIGIT;
         value_m = bigNumber::char2value( value_p );
         if ( bigNumber::invalid == value_m ) {
            type_m = valuesTypes_enum::VALUE_INVALID;
            assert( !"[ERROR] Invalid pushbutton text value" );
         }
      }
   }

   valuesTypes_enum  type () const noexcept { return type_m; }

   bigNumber::element_type value () const noexcept { return value_m; }

private:
   valuesTypes_enum type_m;
   bigNumber::element_type value_m;
};

#endif //#ifndef QT_CALC_BUTTON_H_MRV
