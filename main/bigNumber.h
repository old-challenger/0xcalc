#ifndef BIG_NUMBER_H_MRV
#define BIG_NUMBER_H_MRV

#include <QString>
#include <map>
#include <vector>
#include <cassert>
#include <algorithm>
#include <functional>

class bigNumber {
public:
   using element_type = unsigned char;
   using elements_container = std::vector< element_type >;

   static const element_type  dotMarker = '.',
                              invalid = 0xff;

   static element_type char2value ( char symbol_p ) {
      if ( a_scm <= symbol_p && symbol_p <= f_scm )
         return static_cast< element_type >( symbol_p - a_scm + ten_scm ); // �����
      else if ( zero_scm <= symbol_p && symbol_p <= nine_scm )
         return static_cast< element_type >( symbol_p - zero_scm ); // �����
      else if ( dotMarker == static_cast< element_type >( symbol_p ) )
         return dotMarker;
      else
         return invalid;
   }

   static char value2char ( element_type value_p ) {
      const auto charValue_cl = static_cast< char >( value_p );
      if ( invalid == value_p || dotMarker == value_p)
         return charValue_cl;
      return charValue_cl < ten_scm ? zero_scm + charValue_cl : charValue_cl - ten_scm + a_scm;
   }

   bigNumber ( element_type start_p = 0, bool minus_p = false ) : elements_m{}, dotsPosition_m{ invalidSize_scm }, 
      minus_m{ minus_p }, divisionByZero_m{ false }, period_m{} {
      if ( start_p )
         elements_m.push_back( start_p );
   }

   bigNumber( const bigNumber & etalon_p, bool resetDot_p ) : bigNumber{ etalon_p } { 
      if ( resetDot_p )
         dotsPosition_m = invalidSize_scm; 
   }

   bigNumber( bigNumber && source_p, bool resetDot_p ) : bigNumber{ source_p } {
      if ( resetDot_p )
         dotsPosition_m = invalidSize_scm;
   }

   bool divideByZero () const noexcept { return divisionByZero_m; }

   elements_container::size_type dotsPosition () const noexcept { 
      return invalidSize_scm == dotsPosition_m ? elements_m.size() : dotsPosition_m; 
   }

   const elements_container & elements () const noexcept { return elements_m; }

   bool minus () const noexcept { return minus_m; }

   void rightCut ( elements_container::const_iterator border_p ) {
      elements_m.erase( border_p, elements_m.cend() );
   }

   void setDotsPosition ( elements_container::size_type value_p ) noexcept {
      dotsPosition_m = elements_m.size() == value_p ? invalidSize_scm : value_p;
   }

   void setMinus ( bool flag_p ) {  
      minus_m = elements_m.size() ? flag_p : false;
   }

   void setPeriod ( elements_container::const_iterator begin_p, elements_container::const_iterator end_p ) {
      period_m.clear();
      period_m.insert( period_m.cend(), begin_p, end_p );
   }

   void shift ( element_type next_p ) {
      if ( dotMarker != next_p ) {
         assert( invalid != next_p );
         divisionByZero_m = false;
         elements_m.push_back( next_p );
      } else if ( invalidSize_scm == dotsPosition_m )
         dotsPosition_m = elements_m.size();
   }

   QString text() const;

   bigNumber operator << ( elements_container::size_type steps_p ) const {
      auto result_l = bigNumber{ *this, true };
      const auto newDotsPosition_cl = dotsPosition() + steps_p;
      if ( newDotsPosition_cl > result_l.elements_m.size() )
         result_l.elements_m.insert( result_l.elements_m.cend(), newDotsPosition_cl - result_l.elements_m.size(), 0 );
      result_l.setDotsPosition( newDotsPosition_cl );
      return result_l;
   }

   /*
   bigNumber operator >> ( elements_container::size_type steps_p ) {
      bigNumber result_l( *this );
      return result_l;
   }
   /**/

   bigNumber operator - () const noexcept {
      auto result_l{ *this };
      result_l.minus_m = !result_l.minus_m;
      return result_l;
   }

   bigNumber operator + ( const bigNumber & operand_cp ) const;

   bigNumber operator - ( const bigNumber & operand_cp ) const;

   bigNumber operator * ( element_type element_p ) const {
      auto result_l{ *this };
      multiplyOnElement( result_l.elements_m, element_p );
      return result_l;
   }

   bigNumber operator * ( const bigNumber & operand_cp ) const;

   bigNumber operator / ( const bigNumber & operand_cp ) const;
private:
   using border_type = std::pair< elements_container &, elements_container::size_type >;

   static const elements_container::size_type invalidSize_scm = -1;

   static const char a_scm       = 'A',
                     f_scm       = 'F',
                     zero_scm    = '0',
                     nine_scm    = '9',
                     ten_scm     = '\x0a';

   static const element_type  one_scm           = 0x01,
                              two_scm           = 0x02,
                              fithteen_scm      = 0x0f,
                              oneZero_scm       = 0x10,
                              fithteenZero_scm  = 0xf0; 

   elements_container   elements_m,
                        period_m;
   elements_container::size_type dotsPosition_m;
   bool minus_m, divisionByZero_m;

   static element_type addTo ( elements_container & target_rp, const elements_container & addend_cp );

   static elements_container::size_type deleteZero ( bigNumber & number_rp, bool stopOnDot_p = false );

   template< class container_type >
   static void insertZero ( container_type & container_rp, 
      std::function< typename container_type::const_iterator ( container_type & ) > whereFunction_p,
      typename container_type::size_type zeroSize_p ) {
      container_rp.insert( whereFunction_p( container_rp ), zeroSize_p, 0 );
   }

   template< class container_type >
   static void inverse ( container_type & container_rp ) {
      for ( auto & it : container_rp )
         it ^= fithteen_scm;
   }

   static elements_container::size_type formatContainers ( elements_container::size_type firstDotsPosition_p, 
      elements_container::size_type secondDotsPosition_p, elements_container & first_rp, elements_container & second_rp );

   static bool lessThen ( const elements_container & left_cp, const elements_container & right_cp );

   static border_type makeBorder ( elements_container::size_type firstValue_p, elements_container::size_type secondValue_p,
      elements_container & first_rp, elements_container & second_rp );

   static void multiplyOnElement ( elements_container & container_rp, element_type element_p );

   bigNumber getPartition ( const bigNumber & prefix_cp, 
      elements_container::size_type begin_p, elements_container::size_type end_p ) const;
}; // class bigNumber {
#endif //#ifndef BIH_NUMBER_H_MRV