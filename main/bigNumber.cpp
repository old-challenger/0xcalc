#include "bigNumber.h"
#include "recurseChecker.h"


QString bigNumber::text () const {
   if ( divisionByZero_m )
      return QLatin1String( "[ERROR] division by zero" );
   if ( !elements_m.size() )
      return QLatin1String( "0" );
   QString result_l{ minus_m ? QLatin1String{ "- " } : QString() };
   elements_container::size_type id_l = 0;
   for ( ; id_l < elements_m.size(); ++id_l ) {
      if ( dotsPosition_m == id_l )
         result_l += value2char( dotMarker );
      result_l += value2char( elements_m.at( id_l ) );
   }
   if ( period_m.size() ) {
      result_l += QLatin1String( "(" );
      for ( auto it : period_m )
         result_l += value2char( it );
      result_l += QLatin1String( ")" );
   }
         
   return result_l;
}

bigNumber bigNumber::operator + ( const bigNumber & operand_cp ) const {
   if ( minus_m != operand_cp.minus_m )
      return this->operator-( - operand_cp );
   //
   // ���������� ��������-��������� � ������ ������� - �� �����, �� ��� �����
   auto  result_l = bigNumber{ *this, true };
   auto  operand_l{ operand_cp.elements_m };
   auto  resultDotsPosition_l = formatContainers(  dotsPosition(), operand_cp.dotsPosition(),
                                                   result_l.elements_m, operand_l );
   // �������� � �������������
   const auto overflow_cl = addTo( result_l.elements_m, operand_l );
   // ������������� �����
   if ( overflow_cl ) {
      result_l.elements_m.insert( result_l.elements_m.cbegin(), overflow_cl );
      ++resultDotsPosition_l;
   }
   result_l.setDotsPosition( resultDotsPosition_l );
   result_l.divisionByZero_m = false;
   return result_l;
}

bigNumber bigNumber::operator - ( const bigNumber & operand_cp ) const {
   if ( minus_m != operand_cp.minus_m )
      return this->operator +( - operand_cp );
   // ���������� ��������-��������� � ������ ������� - �� �����, �� ��� �����
   auto result_l = bigNumber{ *this, true };
   if ( !operand_cp.elements_m.size() )
      return result_l;
   auto  operand_l{ operand_cp.elements_m };
   auto  resultDotsPosition_l = formatContainers(  dotsPosition(), operand_cp.dotsPosition(),
                                                   result_l.elements_m, operand_l );
   // �������������� �����
   inverse( operand_l );
   result_l.elements_m.insert( result_l.elements_m.cbegin(), 0 );
   operand_l.insert( operand_l.cbegin(), one_scm );
   addTo( result_l.elements_m, operand_l );
   const auto overflow_cl = result_l.elements_m.front();
   // ������������ �����
   if ( resultDotsPosition_l > 0 )
      result_l.elements_m.erase( result_l.elements_m.cbegin() );
   else
      result_l.elements_m.front() = 0;
   result_l.setDotsPosition( resultDotsPosition_l );
   result_l.divisionByZero_m = false;
   // ������� � ������� �������������
   if ( overflow_cl & two_scm )
      // ������������� ���������
      return std::move( result_l + bigNumber( one_scm, result_l.minus() ) );
   else if ( overflow_cl & one_scm ) {
      // ������������� ���������
      inverse( result_l.elements_m );
      result_l.setMinus( !result_l.minus_m );
   } else
      assert( !"[ERROR] incorrect logic" );
   return std::move( result_l );
}

bigNumber bigNumber::operator * ( const bigNumber & operand_cp ) const {
   const auto newDotsPosition_cl = this->dotsPosition() + operand_cp.dotsPosition(); 
   const bool resultMinus_cl = minus_m != operand_cp.minus_m;
   bigNumber result_l;
   auto offset_l = operand_cp.elements_m.size() - 1;
   for ( auto element : operand_cp.elements_m ) {
      bigNumber currecntResult_l{ *this };
      multiplyOnElement( currecntResult_l.elements_m, element );
      currecntResult_l = currecntResult_l << offset_l;
      result_l = result_l + currecntResult_l;
      --offset_l;
   }
   result_l.setDotsPosition( newDotsPosition_cl ? newDotsPosition_cl - 1 : newDotsPosition_cl );
   deleteZero( result_l, true );
   result_l.divisionByZero_m = false;
   result_l.setMinus( resultMinus_cl );
   return result_l;
}

bigNumber bigNumber::operator / ( const bigNumber & operand_cp ) const {
   const bool resultMinus_cl = minus_m != operand_cp.minus_m;
   bigNumber   divider_l{ operand_cp, true },
               result_l,
               rest_l;
   /// ������ ��� ������������� �����
   const auto  rightShift_cl = dotsPosition(),
               leftShift_cl = operand_cp.dotsPosition() + deleteZero( divider_l, true );
   // ������� �� ����
   if ( !divider_l.elements_m.size() ) {
      result_l.divisionByZero_m = true;
      assert( !result_l.elements_m.size() );
      return result_l;
   }
   auto  begin_l = static_cast< elements_container::size_type >( 0 ), 
         end_l = divider_l.elements_m.size();
   element_type nextResultElement_l;
   recurseChecker< elements_container > finishChecker_l;
   while( finishChecker_l.noFinish() ) {
      // ��������� ����� ����� ��� ���������� �������
      auto currentDividend_l = getPartition( rest_l, begin_l, end_l );
      // ����������� ����� ����� ����������
      if ( lessThen( currentDividend_l.elements_m, divider_l.elements_m ) ) {
         if ( result_l.elements_m.size() )
            result_l.elements_m.push_back( 0 );
         rest_l.elements_m = std::move( currentDividend_l.elements_m );
         assert( divider_l.elements_m.size() == rest_l.elements_m.size() ); 
         begin_l = end_l;
         end_l = begin_l + 1;
      } else {
         assert( divider_l.elements_m.at( 0 ) && currentDividend_l.elements_m.at( 0 ) );
         nextResultElement_l = currentDividend_l.elements_m.at( 0 ) < divider_l.elements_m.at( 0 )
            ? ( ( currentDividend_l.elements_m.at( 0 ) << 4 ) + currentDividend_l.elements_m.at( 1 ) ) / divider_l.elements_m.at( 0 )
            : currentDividend_l.elements_m.at( 0 ) / divider_l.elements_m.at( 0 );
         auto delta_l = divider_l * nextResultElement_l;
         while( lessThen( currentDividend_l.elements_m, delta_l.elements_m ) ) {
            --nextResultElement_l;
            delta_l = divider_l * nextResultElement_l;
         }
         assert( nextResultElement_l );
         result_l.elements_m.push_back( nextResultElement_l );
         // ������� ��������
         rest_l = currentDividend_l - delta_l;
         deleteZero( rest_l, true );
         assert( rest_l.elements_m.size() <= divider_l.elements_m.size() );
         begin_l = end_l;
         end_l = begin_l + divider_l.elements_m.size() - rest_l.elements_m.size();
         // ������� ���������� �����
         finishChecker_l( rest_l, result_l );
      }
   }
   //������������� �����
   if ( leftShift_cl > rightShift_cl ) {
      insertZero< elements_container >( result_l.elements_m,
         []( elements_container & container_p ) { return container_p.cbegin(); }, leftShift_cl - rightShift_cl - 1 );
      result_l.setDotsPosition( 0 );
   } else 
      result_l.setDotsPosition( rightShift_cl - leftShift_cl );
   result_l.setMinus( resultMinus_cl );
   return result_l;
}

bigNumber::element_type bigNumber::addTo ( elements_container & target_rp, const elements_container & addend_cp ) {
   element_type   overflow_l = 0,
                  sum_l = 0;
   for ( auto id = target_rp.size(); id; ) {
      --id;
      sum_l = target_rp.at( id ) + addend_cp.at( id ) + overflow_l;
      target_rp[ id ] = sum_l & fithteen_scm;
      overflow_l = ( sum_l & oneZero_scm ) >> 4;
      assert( ( ( sum_l & fithteenZero_scm ) >> 4 ) == overflow_l );
   }
   return overflow_l;
}

bigNumber::elements_container::size_type bigNumber::deleteZero ( bigNumber & number_rp, bool stopOnDot_p ) {
   const auto dotsPosition_cl = number_rp.dotsPosition();
   // �������� ����� �����
   auto stopPosition_l = stopOnDot_p ? dotsPosition_cl : number_rp.elements_m.size();
   elements_container::size_type leftNotNullPosition_l = 0;
   while ( leftNotNullPosition_l < stopPosition_l && !number_rp.elements_m.at( leftNotNullPosition_l ) )
      ++leftNotNullPosition_l;
   number_rp.elements_m.erase( number_rp.elements_m.cbegin(), number_rp.elements_m.cbegin() + leftNotNullPosition_l );
   // �������� ����� ������
   if ( !number_rp.elements_m.size() )
      return leftNotNullPosition_l;
   stopPosition_l = stopOnDot_p ? dotsPosition_cl : 1;
   auto rightNotNullPosition_l = number_rp.elements_m.size();
   while ( stopPosition_l < rightNotNullPosition_l && !number_rp.elements_m.at( --rightNotNullPosition_l ) ) ;
   number_rp.elements_m.erase( number_rp.elements_m.cbegin() + rightNotNullPosition_l, number_rp.elements_m.cend() );
   number_rp.setDotsPosition( leftNotNullPosition_l < dotsPosition_cl ? dotsPosition_cl - leftNotNullPosition_l : 0 );
   return leftNotNullPosition_l;
}

bigNumber::elements_container::size_type bigNumber::formatContainers (  elements_container::size_type firstDotsPosition_p,
   elements_container::size_type secondDotsPosition_p, elements_container & first_rp, elements_container & second_rp ) {
   auto  start_l  = makeBorder( firstDotsPosition_p, secondDotsPosition_p, first_rp, second_rp ),
         stop_l   = makeBorder( first_rp.size() - firstDotsPosition_p, second_rp.size() - secondDotsPosition_p, first_rp, second_rp );
   insertZero< elements_container >( start_l.first,
      []( elements_container & container_p ) { return container_p.cbegin(); }, start_l.second );
   insertZero< elements_container >( stop_l.first,
      []( elements_container& container_p ) { return container_p.cend(); }, stop_l.second );
   assert( first_rp.size() == second_rp.size() );
   return std::max( firstDotsPosition_p, secondDotsPosition_p );
}

bool bigNumber::lessThen ( const elements_container & left_cp, const elements_container & right_cp ) {
   if ( left_cp.size() < right_cp.size() )
      return true;
   if ( right_cp.size() < left_cp.size() )
      return false;
   assert( left_cp.size() == right_cp.size() );
   for ( elements_container::size_type id = 0; id < right_cp.size(); ++id ) {
      if ( left_cp.at( id ) < right_cp.at( id ) )
         return true;
      if ( right_cp.at( id ) < left_cp.at( id ) )
         return false;
   }
   return false;
}

bigNumber::border_type bigNumber::makeBorder ( elements_container::size_type firstValue_p, elements_container::size_type secondValue_p,
   elements_container & first_rp, elements_container & second_rp ) {
   return ( firstValue_p < secondValue_p )
      ? border_type( first_rp, secondValue_p - firstValue_p )
      : border_type( second_rp, firstValue_p - secondValue_p );
}

void bigNumber::multiplyOnElement ( elements_container & container_rp, element_type element_p ) {
   element_type   overflow_l        = 0,
                  multiplication_l  = 0;
   for ( auto it = container_rp.rbegin(); container_rp.rend() != it; ++it ) {
      multiplication_l = *it * element_p + overflow_l;
      *it = multiplication_l & fithteen_scm;
      overflow_l = ( multiplication_l & fithteenZero_scm ) >> 4;
   }
   if ( overflow_l )
      container_rp.insert( container_rp.cbegin(), overflow_l );
}

bigNumber bigNumber::getPartition ( const bigNumber & prefix_cp,
   elements_container::size_type begin_p, elements_container::size_type end_p ) const {
   bigNumber result_l{ prefix_cp };
   result_l.elements_m.reserve( prefix_cp.elements_m.size() + end_p - begin_p );
   if ( begin_p < elements_m.size() )
      result_l.elements_m.insert(
         result_l.elements_m.cend(),
         elements_m.cbegin() + begin_p,
         elements_m.cbegin() + std::min( end_p, elements_m.size() ) );
   if ( end_p > elements_m.size() )
      result_l.elements_m.insert( result_l.elements_m.cend(),
         end_p - std::max( elements_m.size(), begin_p ), 0 );
   return result_l;
}