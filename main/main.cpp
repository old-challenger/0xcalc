#include "QMainWidget.h"

#include <QApplication>
#include <memory>

int main ( int argc, char ** argv ) {
   QApplication app( argc, argv );
   QMainWidget::widget_ptr< QMainWidget > main_l{ new QMainWidget{}, &QMainWidget::deleteWidget };
   main_l->show();
   return app.exec();
}