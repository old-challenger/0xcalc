#ifndef RECURSE_CHECKER_H_MRV
#define RECURSE_CHECKER_H_MRV

#include "bigNumber.h"

template< class container_type >
class recurseChecker {
public:
   recurseChecker () : restsIndex_m{}, noFinish_m{ true } { ; }

   bool noFinish () const noexcept { return noFinish_m; }
   
   bool operator () ( const bigNumber & rest_cp, bigNumber & result_rp ) {
      if ( !rest_cp.elements().size() )
         return noFinish_m = false;
      auto found_l = restsIndex_m.lower_bound( rest_cp.elements() );
      if ( restsIndex_m.end() == found_l || found_l->first != rest_cp.elements() )
         restsIndex_m.insert( found_l, std::make_pair( rest_cp.elements(), result_rp.elements().size() ) );
      else {
         const auto sequenceSize_cl = result_rp.elements().size() - found_l->second;
         const auto  secondOffset_cl = found_l->second,
                     firstOffset_cl = secondOffset_cl - sequenceSize_cl;
         found_l->second = result_rp.elements().size(); 
         if ( sequenceSize_cl * 2 > result_rp.elements().size() )
            return noFinish_m = true;
         auto  firstBegin_l = result_rp.elements().cbegin() + firstOffset_cl,
               secondBegin_l = result_rp.elements().cbegin() + secondOffset_cl;
         if ( std::equal( firstBegin_l, secondBegin_l, secondBegin_l, result_rp.elements().cend() ) ) {
            result_rp.setPeriod( secondBegin_l, result_rp.elements().cend() );
            result_rp.rightCut( firstBegin_l );
            return noFinish_m = false;
         }
      }
      return noFinish_m = true;
   }
private:
   std::map< container_type, typename container_type::size_type > restsIndex_m;
   bool noFinish_m;
};

#endif //RECURSE_CHECKER_H_MRV


