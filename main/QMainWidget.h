#ifndef QT_MAIN_WIDGET_H_MRV
#define QT_MAIN_WIDGET_H_MRV

#include "QCalcButton.h"
#include <QFrame>
#include <vector>
#include <memory>
#include <map>

class QLabel;
class QGridLayout;

class QMainWidget : public QFrame {
Q_OBJECT
public:
   template< class widget_type >
   using widget_ptr = std::unique_ptr < widget_type, decltype( &QMainWidget::deleteWidget ) >;

   QMainWidget();

   static void deleteWidget( QWidget * widget_p ) { widget_p->deleteLater(); }
private:
   using buttonsContainer_type = std::vector< widget_ptr< QCalcButton > >;

   bigNumber current_m, buffer_m;
   QCalcButton::valuesTypes_enum operation_m;
   QLabel * outField_m;
   buttonsContainer_type buttons_m;
   std::map< QCalcButton *, buttonsContainer_type::size_type > buttonsDict_m;

   void addButton( QGridLayout * layout_p, char value_p, int column_p, int row_p );
private slots:
   void sltOnButtonPush ();
};

#endif //#ifndef QT_MAIN_WIDGET_H_MRV