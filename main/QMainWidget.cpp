#include "QMainWidget.h"

#include <QGridLayout>
#include <QLabel>

QMainWidget::QMainWidget() : QFrame{ nullptr }, current_m{}, buffer_m{}, 
   operation_m{ QCalcButton::valuesTypes_enum::VALUE_RESULT }, outField_m{ nullptr } {
   auto * const mainLayout_l = new QVBoxLayout{ this };
   auto * const buttonsLayout_l = new QGridLayout{};
   
   // ����������� - �������� ����� �������� ����������
   buttons_m.reserve( 22 );
   addButton( buttonsLayout_l, 'A', 0, 0 );
   addButton( buttonsLayout_l, 'B', 0, 1 );
   addButton( buttonsLayout_l, 'C', 1, 0 );
   addButton( buttonsLayout_l, 'D', 1, 1 );
   addButton( buttonsLayout_l, 'E', 2, 0 );
   addButton( buttonsLayout_l, 'F', 2, 1 );

   addButton( buttonsLayout_l, '0', 3, 2 );
   addButton( buttonsLayout_l, '1', 2, 2 );
   addButton( buttonsLayout_l, '2', 2, 3 );
   addButton( buttonsLayout_l, '3', 2, 4 );
   addButton( buttonsLayout_l, '4', 1, 2 );
   addButton( buttonsLayout_l, '5', 1, 3 );
   addButton( buttonsLayout_l, '6', 1, 4 );
   addButton( buttonsLayout_l, '7', 0, 2 );
   addButton( buttonsLayout_l, '8', 0, 3 );
   addButton( buttonsLayout_l, '9', 0, 4 );

   addButton( buttonsLayout_l, '+', 0, 5 );
   addButton( buttonsLayout_l, '-', 1, 5 );
   addButton( buttonsLayout_l, '*', 2, 5 );
   addButton( buttonsLayout_l, '/', 3, 5 );
   addButton( buttonsLayout_l, '=', 3, 4 );
   addButton( buttonsLayout_l, '.', 3, 3 );

   //
   mainLayout_l->addWidget( outField_m = new QLabel( current_m.text(), this ) );
      outField_m->setAlignment( Qt::Alignment::enum_type::AlignRight );
      outField_m->setFrameStyle( QFrame::Box | QFrame::Plain );
   mainLayout_l->addLayout( buttonsLayout_l );
   //
   setWindowTitle( tr( "hex-calculator" ) );
}

void QMainWidget::addButton( QGridLayout * layout_p, char value_p, int column_p, int row_p ) {
   auto * const button_l = new QCalcButton( value_p, this );
   buttonsDict_m.insert( std::make_pair( button_l, buttons_m.size() ) );
   buttons_m.emplace_back( button_l, &deleteWidget );
   layout_p->addWidget( button_l, column_p, row_p );
   connect( button_l, SIGNAL( released() ), this, SLOT( sltOnButtonPush() ) );
}

void QMainWidget::sltOnButtonPush() {
   auto * const sender_l = dynamic_cast< QCalcButton * >( sender() );
   assert( nullptr != sender_l );
   const auto found_l = buttonsDict_m.find( sender_l );
   assert( buttonsDict_m.end() != found_l );
   const auto & currentButton_cl = buttons_m.at( found_l->second );
   if ( QCalcButton::valuesTypes_enum::VALUE_DIGIT == currentButton_cl->type() )
      current_m.shift( currentButton_cl->value() );
   else {
      if ( QCalcButton::valuesTypes_enum::VALUE_RESULT != operation_m ){
         switch ( operation_m ) {
         case QCalcButton::valuesTypes_enum::VALUE_PLUS:
            current_m = buffer_m + current_m;
         break;
         case QCalcButton::valuesTypes_enum::VALUE_MINUS:
            current_m = buffer_m - current_m;
         break;
         case QCalcButton::valuesTypes_enum::VALUE_MULTIPLY:
            current_m = buffer_m * current_m;
         break;
         case QCalcButton::valuesTypes_enum::VALUE_DIVIDE:
            current_m = buffer_m / current_m;
         break;
         default:
            assert(!"[ERROR] Incorrect swicth branch");
         }
         operation_m = QCalcButton::valuesTypes_enum::VALUE_RESULT;
         buffer_m = bigNumber{};
      }
      if ( QCalcButton::valuesTypes_enum::VALUE_RESULT != currentButton_cl->type() ) {
         buffer_m = current_m;
         current_m = bigNumber{};
         operation_m = currentButton_cl->type();
      }
   }
   outField_m->setText(current_m.text());
}